#pragma once
#include <iostream>
#include <iomanip>
#include <QString>

// Converts a floating point number to a string removing trailing zeros
QString fixed_float(double x)
{
    std::ostringstream ss;
    ss << std::fixed << std::setprecision(std::cout.precision()) << x;
    std::string str = ss.str();

    int last = str.find_last_not_of('0') + 1;
    if (str[last - 1] == '.') last--;
    return QString(str.substr(0, last).c_str());
}
