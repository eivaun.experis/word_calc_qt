#include <QCoreApplication>
#include "calc.h"
#include "fixed_float.h"

int main(int argc, char *argv[])
{
    QTextStream in(stdin);
    QTextStream out(stdout);

    // Get the input.
    QString input;
    if (argc >= 2)
    {
        input = argv[1];
    }
    else
    {
        out << "Enter equation: ";
        out.flush();
        input = in.readLine();
    }

    // Split at spaces.
    const QStringList parts = input.split(' ');

    // Which part we're at. The cursor is moved forward by the parse functions
    // such that the next parser can continue where the previous one ended.
    qsizetype cursor = 0;

    // Parse.
    try
    {
        const double num1 = parse_number(parts, cursor);
        const Operator op = parse_operator(parts, cursor);
        const double num2 = parse_number(parts, cursor);

        // Calculate the result.
        const double res = calc(num1, num2, op);

        // Print with numbers and symbols
        out << fixed_float(num1) << " " << operator_to_symbol(op) << " " << fixed_float(num2) << " = "
                  << fixed_float(res) << '\n';
        out.flush();

        // Print with text
        out << number_to_string(num1) << " " << operator_to_string(op) << " " << number_to_string(num2) << " equals "
                  << number_to_string(res) << '\n';
        out.flush();
    }
    catch (const std::exception& e)
    {
        qFatal("%s", e.what());
    }

    return 0;
}
