#pragma once

#include <vector>
#include <QString>
#include <QStringList>

enum class Operator
{
    add,
    sub,
    mul,
    div
};

// The maximum number of decimals we print
constexpr size_t max_decimals = 3;

// Tries to parse a single 999 block of a number
// E.g: "four hundred seventy six thousand two hundred four"(476,204) is parsed as 476 and the cursor is moved to "thousand"
double parse_hundred(const QStringList& parts, qsizetype& cursor);

// Tries to parse a number
double parse_number(const QStringList& parts, qsizetype& cursor);

// Tries to parse the operator.
Operator parse_operator(const QStringList& parts, qsizetype& cursor);

// Calculates the result based on the given operator.
double calc(double a, double b, Operator op);

// Converts a operator to a text symbol
QString operator_to_symbol(Operator op);

// Converts a operator to text
QString operator_to_string(Operator op);

// Converts a number into words
QString number_to_string(double number);
